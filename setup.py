# -*- coding: utf-8 -*-

import os

from setuptools import setup

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__))

requirements = []
with open(os.path.join(PROJECT_ROOT, 'requirements.txt')) as f:
    for line in f:
        line = line.strip()
        if not line and line.startswith('#') or line.startswith('-e'):
            # Игнорируем пустые строки, комментарии и пакеты из репозиториев (-e)
            continue

        requirements.append(line)

setup(
    name='opensky_wrapper',
    version='0.1',
    description='Get planes around target by GPS.',
    url='https://bitbucket.org/alexander_nazarov/opensky_wrapper',
    author='Alexander Nazarov',
    author_email='aonazarov@gmail.com',
    license='MIT',
    packages=['opensky_wrapper'],
    zip_safe=False,
    install_requires=requirements,

    test_suite='nose.collector',
    tests_require=['nose'],

    # Чтобы случайно не зарелизить в публичный PyPI
    classifiers=['Private :: Do Not Upload'],
)
