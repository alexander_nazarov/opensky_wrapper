# README #

Test library to get info about objects around target by GPS-coordinates.
Uses OpenSky API.

### How do I get set up? ###

```
pip install git+https://alexander_nazarov@bitbucket.org/alexander_nazarov/opensky_wrapper.git
```

### Usage ###

* get planes around Paris
```
from opensky_wrapper import find_around_paris, find_around_paris_in_donut

# получим список самолетов в `пончике`, то есть на расстоянии не ближе 400км от Парижа и не далее 500км
for i in find_around_paris_in_donut(450, 50):
	print(i)
	
# получим список самолетов в окружности вокруг Парижа, то есть на расстоянии не далее 500км
for i in find_around_paris(450, 50):
	print(i)
```

### Tests ####

```
python setup.py test
```

### Замечения, мысли ###

* непонятно зачем для 1го метода городить библиотеку (видимо чисто ради теста)
* сделал 2 метода, так как непонятно в чем сложность задания
* если бы был другой бизнес-процесс, то было бы эффективнее хранить объекты в БД и средствами СУБД делать выборку (ПОСТГИС) (например, это справедливо для задачи, поиска квартир/объектов вокруг метро и т.п. )
* у OpenSky есть своя питон-библиотека, но ради 1го метода нагружать завимисотью сторонней библиотекой, не захотелось
* использовал ujson, так как она производительнее работает с json
* geopy - в принципе использовал ради 1 формулы и из-за более точного расчета по радиусу Земли