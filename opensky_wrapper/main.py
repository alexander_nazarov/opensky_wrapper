# -*- coding: utf-8 -*-

import requests
import ujson
from geopy.distance import vincenty

# Paris GPS coordinates
PARIS = (48.85341, 2.3488)
# API URL METHOD (see https://opensky-network.org/apidoc/rest.html)
SOURCE_URL = 'https://opensky-network.org/api/states/all'


def _get_parsed_data():
    """
    Loads data from OpenSky API and returns parsed json data.
    
    :rtype: list(list)
    :return: parsed json data
    """
    resp = requests.get(SOURCE_URL)

    if not resp.ok:
        raise Exception('Bad response from OpenSky API.')

    try:
        resp_data = ujson.loads(resp.content)
    except ValueError:
        raise Exception('Bad data in response from OpenSky API.')

    if not isinstance(resp_data, dict):
        raise Exception('Bad data in response from OpenSky API.')

    states = resp_data.get('states')
    if not isinstance(states, list):
        raise Exception('Bad data in response from OpenSky API.')

    return states


def _find_by_radius(target_latitude, target_longitude, radius, offset=0, in_donut=False):
    """
    Finds objects inside circle by radius (+_ offset) by GPS-coordinates.
    If you want to get objects that are in distance like `radius-offset <= distance <= radius+offset` 
    then use `in_donut == True`.

    :param float target_latitude: 
    :param float target_longitude:
    :param float radius: in kilometers
    :param float offset: possible offset in kilometers
    :param boolean in_donut: if False - find objects in circle, else - in "donut".
    :rtype: generator
    :return: [(<object callsign>, <distance between object and target>), ...]
    """

    if radius <= 0 or offset < 0:
        raise Exception('Bad args for calculating distance.')

    if in_donut:
        if offset > radius:
            raise Exception('Bad args for calculating distance.')

        min_distance = radius - offset
    else:
        min_distance = 0

    max_distance = radius + offset

    for state in _get_parsed_data():
        try:
            callsign = state[1]
            longitude = state[5]
            latitude = state[6]
        except IndexError:
            raise Exception('Wrong state format.')

        distance = vincenty((target_latitude, target_longitude), (latitude, longitude)).kilometers

        if min_distance <= distance <= max_distance:
            yield (callsign, distance)


def find_by_radius(target_latitude, target_longitude, radius, offset=0, in_donut=False):
    """
    See `_find_by_radius` specification.
    Return whole list of elements.
    
    :param float target_latitude: 
    :param float target_longitude:
    :param float radius: in kilometers
    :param float offset: possible offset in kilometers
    :param boolean in_donut: if False - find objects in circle, else - in "donut".
    :rtype: list(set)
    :return: [(<object callsign>, <distance between object and target>), ...]
    """
    return [i for i in _find_by_radius(target_latitude, target_longitude, radius, offset, in_donut)]


def ifind_by_radius(target_latitude, target_longitude, radius, offset=0, in_donut=False):
    """
    See `_find_by_radius` specification.
    Return generator.
    
    :param float target_latitude: 
    :param float target_longitude:
    :param float radius: in kilometers
    :param float offset: possible offset in kilometers
    :param boolean in_donut: if False - find objects in circle, else - in "donut".
    :rtype: generator
    :return: [(<object callsign>, <distance between object and target>), ...]
    """
    return _find_by_radius(target_latitude, target_longitude, radius, offset, in_donut)


def find_around_paris(radius, offset):
    """
    Find objects around Paris if distance `radius+offset`.
    :param float radius: 
    :param float offset: 
    :rtype: generator
    :return: [(<object callsign>, <distance between object and target>), ...]
    """
    return ifind_by_radius(
        target_latitude=PARIS[0],
        target_longitude=PARIS[1],
        radius=radius,
        offset=offset
    )


def find_around_paris_in_donut(radius, offset):
    """
    Find objects around Paris in distance not closer than `radius-offset`.
    
    :param float radius: 
    :param float offset: 
    :rtype: generator
    :return: [(<object callsign>, <distance between object and target>), ...]
    """
    return ifind_by_radius(
        target_latitude=PARIS[0],
        target_longitude=PARIS[1],
        radius=radius,
        offset=offset,
        in_donut=True
    )

if __name__ == '__main__':

    for i in find_around_paris_in_donut(450, 50):
        print(i)
