# -*- coding: utf-8 -*-

import types
import ujson
import unittest

from mock import patch

from opensky_wrapper import find_by_radius, ifind_by_radius, find_around_paris, find_around_paris_in_donut
from opensky_wrapper.main import _get_parsed_data, _find_by_radius

PARIS = (48.85341, 2.3488)
SOURCE_URL = 'https://opensky-network.org/api/states/all'


API_PARSED_DATA = [
    [u'89906e', u'EVA266  ', u'Taiwan', 1503305301, 1503305318, 121.0487, 24.9411, 4853.94],
    [u'aa5b9f', u'ABX982  ', u'United States', 1503305319, 1503305319, -87.3808, 37.0795, 10972.8],
    [u'a0cfbd', u'AAL898  ', u'United States', 1503305294, 1503305313, -96.8451, 38.2442, 10675.62],
    [u'3cc1c8', u'DCARO   ', u'Germany', 1503306979, 1503306979, 6.4526, 52.169200000000004, 12192],
    [u'4b1902', u'EDW15J  ', u'Switzerland', 1503306979, 1503306979, 8.1027, 47.4614, 3924.3],
    [u'4b1900', u'SWR1575 ', u'Switzerland', 1503306979, 1503306979, 8.5584, 47.4553, None]
]


class FakeResponse(object):
    """
    Фейковый класс для тестирования работы с API.
    """

    def __init__(self, status_code, content):
        self.status_code = status_code
        self.content = ujson.dumps(content)

    @property
    def ok(self):
        return True if self.status_code == 200 else False


class TestLib(unittest.TestCase):

    @patch('opensky_wrapper.main._get_parsed_data')
    def test_common_return_types(self, mock_func):
        """
        Common tests to check return types from functions.
        """

        mock_func.return_value = API_PARSED_DATA

        # дано: корректный вызов функции, функция корректно выполнилась
        # ожидается: тип результата - list
        res = find_by_radius(10.01, 11.02, 450, 50)
        self.assertIsInstance(res, list)

        # дано: корректный вызов функции, функция корректно выполнилась
        # ожидается: тип результата - generator
        res = ifind_by_radius(10.01, 11.02, 450, 50)
        self.assertIsInstance(res, types.GeneratorType)

        # дано: корректный вызов функции, функция корректно выполнилась
        # ожидается: тип результата - generator
        res = find_around_paris(450, 50)
        self.assertIsInstance(res, types.GeneratorType)

        # дано: корректный вызов функции, функция корректно выполнилась
        # ожидается: тип результата - generator
        res = find_around_paris_in_donut(450, 50)
        self.assertIsInstance(res, types.GeneratorType)

    @patch('requests.get')
    def test_api_data_processing(self, mock_func):
        """
        Tests for `opensky_wrapper.main._get_parsed_data` function.
        """

        # дано: АПИ вернуло корректные данные
        # ожидается: функция вернула данные о состоянии объектов
        mock_func.return_value = FakeResponse(200, {'states': API_PARSED_DATA})
        res = _get_parsed_data()
        self.assertEqual(res, API_PARSED_DATA)

        # дано: HTTP ошибка
        # ожидается: функция выбросит исключение
        mock_func.return_value = FakeResponse(404, {'states': API_PARSED_DATA})
        with self.assertRaises(Exception) as context:
             _get_parsed_data()
        self.assertEqual(context.exception.message, 'Bad response from OpenSky API.')

        # дано: API вернуло невалидный json
        # ожидается: функция выбросит исключение
        mock_func.return_value = FakeResponse(200, '<bad json>')
        with self.assertRaises(Exception) as context:
            _get_parsed_data()
        self.assertEqual(context.exception.message, 'Bad data in response from OpenSky API.')

        # дано: API вернуло валидный json, но неверный формат данных (например, список вместо словаря)
        # ожидается: функция выбросит исключение
        mock_func.return_value = FakeResponse(200, [1, 2, 3])
        with self.assertRaises(Exception) as context:
            _get_parsed_data()
        self.assertEqual(context.exception.message, 'Bad data in response from OpenSky API.')

        # дано: API вернуло валидный json, данные в виде словаря, но нет данных по ключу `states`
        # ожидается: функция выбросит исключение
        mock_func.return_value = FakeResponse(200, {'missed_states': API_PARSED_DATA})
        with self.assertRaises(Exception) as context:
            _get_parsed_data()
        self.assertEqual(context.exception.message, 'Bad data in response from OpenSky API.')

    @patch('opensky_wrapper.main._get_parsed_data')
    def test_find_by_radius(self, mock_func):
        """
        Test data processing of `_find_by_radius` function.        
        """
        mock_func.return_value = API_PARSED_DATA

        # дано: передали неверные данные для расчета радиуса (отрицательные значения для radius и offset)
        # ожидается: функция выбросит исключение
        bad_args = [(-50, 50), (-50, -50), (50, -50)]
        for radius, offset in bad_args:
            generator_res = _find_by_radius(
                target_latitude=10.01,
                target_longitude=11.02,
                radius=-50,
                offset=450
            )
            with self.assertRaises(Exception) as context:
                [i for i in generator_res]
            self.assertEqual(context.exception.message, 'Bad args for calculating distance.')

        # дано: передали неверные данные для расчета радиуса (оффсет первышает радиус при поиске самолетов в `пончике`)
        # ожидается: функция выбросит исключение
        generator_res = _find_by_radius(
            target_latitude=10.01,
            target_longitude=11.02,
            radius=50,
            offset=150,
            in_donut=True
        )
        with self.assertRaises(Exception) as context:
            [i for i in generator_res]
        self.assertEqual(context.exception.message, 'Bad args for calculating distance.')

        # дано: АПИ вернуло данные с некорректным форматом.
        # ожидается: функция выбросит исключение
        mock_func.return_value = [['', '', '', '']]
        generator_res = _find_by_radius(
            target_latitude=10.01,
            target_longitude=11.02,
            radius=50,
            offset=450
        )
        with self.assertRaises(Exception) as context:
            [i for i in generator_res]
        self.assertEqual(context.exception.message, 'Wrong state format.')


    @patch('opensky_wrapper.main._get_parsed_data')
    def test_objects_find(self, mock_func):
        """
        Tests than search with `in_donut` option gives correct results. 
        """

        mock_func.return_value = API_PARSED_DATA

        # дано: в заданном радиусе не нашлось объектов
        # ожидается: пустой список
        generator_res = _find_by_radius(
            target_latitude=10.01,
            target_longitude=11.02,
            radius=50,
            offset=150,
        )
        res = [i for i in generator_res]
        self.assertEqual(res, [])

        # дано: в заданном радиусе нашли 3 объекта (для radius==450, offset==50)
        # ожидается: список с информацией для 3х объектов
        generator_res = find_around_paris(
            radius=450,
            offset=50
        )
        res = [i for i in generator_res]
        self.assertEqual(len(res), 3)

        # дано: в заданном радиусе нашли 1 объект (для radius==450, offset==16 - для того,
        # чтобы результат отличался от предыдущего теста)
        # ожидается: список с информацией для 1го объекта
        generator_res = find_around_paris_in_donut(
            radius=450,
            offset=16
        )
        res = [i for i in generator_res]
        self.assertEqual(len(res), 1)


if __name__ == '__main__':
    unittest.main()
